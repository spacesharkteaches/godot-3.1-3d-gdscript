extends Camera

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var player_path = NodePath()
var player

export var follow_offset = Vector3(0, 5, 10)
export var follow_speed = 6

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node(player_path)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var target_position = player.transform.origin + follow_offset
	target_position = lerp(transform.origin, target_position, delta * follow_speed)
	transform.origin = target_position

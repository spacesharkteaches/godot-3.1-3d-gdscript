extends RigidBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var rotational_speed = 45
export var max_speed = 15

var movement_input = Vector3()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	
func process_input(delta):
	movement_input = Vector3.ZERO
	
	if Input.is_action_pressed("movement_forward"):
		movement_input.z -= 1
		
	if Input.is_action_pressed("movement_backward"):
		movement_input.z += 1
		
	if Input.is_action_pressed("movement_left"):
		movement_input.x -= 1
		
	if Input.is_action_pressed("movement_right"):
		movement_input.x += 1
		
	movement_input = movement_input.normalized() * delta * rotational_speed
	
func process_movement(delta):
	# apply_impulse(Vector3.ZERO, movement_input)
	
	if(abs(linear_velocity.x) > max_speed):
		movement_input.x = 0
	if(abs(linear_velocity.z) > max_speed):
		movement_input.z = 0
	
	var torque = Vector3(movement_input.z, 0, -movement_input.x)
	apply_torque_impulse(torque)
